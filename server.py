from game import Game
import db

from sanic import Sanic
from sanic.response import file

import socketio
import asyncio
import uvloop

sio = socketio.AsyncServer(async_mode='sanic')
app = Sanic(__name__)
sio.attach(app)

game = Game(sio)

@app.route('/')
async def index(request):
	return await file('client/index.html')

app.static('/', './client')

@sio.on('login')
async def login(sid, data):
	logged = db.login(data['username'], data['password'])
	if not logged:
		return await sio.emit('message', {
			'text': 'Incorrect username or password',
			'icon': 'error' })

	await sio.emit('message', {
		'text': 'You are logged in',
		'icon': 'success' })

	uid = db.userID(data['username'])

	game.addPlayer(uid, sid);

	await sio.emit('loggedIn');

@sio.on('register')
async def register(sid, data):
	if db.userID(data['username']) > 0:
		return await sio.emit('message', {
			'text': 'User with that name already exists',
			'icon': 'error' })

	db.createUser(data['username'], data['password'])

	await sio.emit('message', {
			'text': 'Now you can sign in',
			'icon': 'success' })

@sio.on('connect')
async def connect(sid, environ):
	print(sio)
	print(sid, 'connected')

@sio.on('disconnect')
def disconnect(sid):
	game.removePlayer(sid);
	print(sid, 'disconnected')

server = app.create_server(host="0.0.0.0", port=8000, log_config=None)

asyncio.set_event_loop(uvloop.new_event_loop())
loop = asyncio.get_event_loop()

asyncio.ensure_future(game.loop())
asyncio.ensure_future(server)

try:
	loop.run_forever()
except:
	loop.stop()
