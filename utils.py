import random

import hashlib

import base64
import os

alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def genPassword(length):
	password = ""
	for i in range(length):
		index = random.randrange(len(alphabet))
		password += alphabet[index]
	return password

def genID():
	return base64.urlsafe_b64encode(os.urandom(24)).decode('ascii')

def hash(string):
	return hashlib.md5( string.encode('utf-8') ).hexdigest()