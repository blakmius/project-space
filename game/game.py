import time
import asyncio
from .player import Player

class Error(Exception):
	def __init__(self, name):
		self.name = name

	def __str__(self):
		return repr(self.name)

class Clock(object):
	def __init__(self):
		self.rate = 30
		self.interval = 1./self.rate
		self.lastTick = time.time()

	async def tick(self):
		currentTime = time.time()
		dt = currentTime - self.lastTick

		sleepTime = self.interval - dt
		if sleepTime > 0:
			await asyncio.sleep(sleepTime)

			currentTime = time.time()
			dt = currentTime - self.lastTick

		self.lastTick = currentTime

		return dt

class Game(object):
	def __init__(self, socket):
		self.clock = Clock()
		self.players = []
		self.socket = socket

	def addPlayer(self, id, sid):
		for player in self.players:
			if player.id == id:
				#await self.socket.send()
				raise Error('Player already in game')

		self.players.append(Player(id, sid))

	def removePlayer(self, sid):
		for player in self.players:
			if player.sid == sid: self.players.remove(player)

	def update(self, dt):
		pass#print(dt)

	async def loop(self):
		while True:
			dt = await self.clock.tick()
			self.update(dt)