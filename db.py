import sqlite3
import utils
import time
import os

dbExists = os.path.exists('database.db')

database = sqlite3.connect('database.db')
database.row_factory = sqlite3.Row

def createEmptyDB():
	for i in open('database.sql', 'r').read().split(';'):
		database.execute(i)

if not dbExists: createEmptyDB()

class _execute(object):
	def __init__(self, command, *params):
		cursor = database.cursor()
		cursor.execute(command, tuple(params))

		self.data = [el for el in cursor]
		self.empty = len(self.data) == 0

		database.commit()

	def __getitem__(self, i):
		return self.data[i]

#User

def login(username, password):
	userpass = _execute('SELECT password FROM User WHERE name=(?)', username)
	if userpass.empty or userpass[0]['password'] != utils.hash(password): return False
	return True

def userID(username):
	user = _execute('SELECT id FROM User WHERE name=(?)', username)
	if user.empty: return -1
	return user[0]['id']

def createUser(username, password):
	_execute('INSERT INTO User (name, password) VALUES (?,?)', username, utils.hash(password))