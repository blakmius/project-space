require(['game/tileset', 'game/gui'], (Tileset, gui) => {

const size = 64;

const app = new PIXI.Application(window.innerWidth, window.innerHeight);
document.body.appendChild(app.view);

$(window).resize(e => app.renderer.resize(window.innerWidth, window.innerHeight));

let tilemap = new PIXI.tilemap.CompositeRectTileLayer(0, [], true);
let tileset = new Tileset('assets/Tileset.png', size);

app.stage.addChild(tilemap);

app.ticker.add(dt => {
	tilemap.clear();

	for (let y=0; y<5; y++) {
		for (let x=0; x<8; x++) {
			tilemap.addFrame(tileset.get(x, y), x*size, y*size);
		}
	}
});

});