define(['./socket', './cookie'], (socket, cookie) => {

const app = new Vue({
	el: '#gui',
	data: {
		state: 'auth',
		auth: {
			panel: 'login',
			credentials: {
				username: '',
				password: ''
			},
			register: {
				username: '',
				password: '',
				repeatPassword: ''
			},
			Login() {
				socket.emit('login', this.credentials);
				this.credentials.username = '';
				this.credentials.password = '';
			},
			Register() {
				let {username, password, repeatPassword} = this.register;
				if (password != repeatPassword) return $.toast({
					text: 'Passwords don`t match',
					icon: 'warning',
					heading: 'Warning',
					position: 'top-right'
				});
				socket.emit('register', {username, password});
				this.register.username = '';
				this.register.password = '';
				this.register.repeatPassword = '';
			}
		}
	}
});

$('#gui').removeClass('hidden');

socket.on('loggedIn', () => {
	app.state = 'game'
})

return app;

});