define([], ()=>{

function get(name) {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        var sep = cookie.indexOf('=');
        var _name = cookie.substr(0, sep);
        var value = cookie.substr(sep+1);
        var start = (value.length > 0 && value[0] == '"') ? 1 : 0;
        var end = (value.length > 0 && value[value.length-1] == '"') ? value.length-1 : value.length;
        var _value = value.substring(start, end); // substr and substring are different functions
        if (_name === name) return _value;
    }
    return '';
}

function set(name, value, lifetime) {
    var d = new Date();
    d.setTime(d.getTime() + lifetime*1000);
    var expires = "expires="+ d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

return {get, set};

});