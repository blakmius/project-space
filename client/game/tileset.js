define([], ()=>{

class Tileset {
	constructor(filename, size) {
		this.size = size;
		this.texture = new PIXI.Texture.fromImage(filename);
		this.cache = {};
	}

	get(x, y, w=1, h=1) {
		let name = `${x}_${y}_${w}_${h}`;
		if (!this.cache[name]) {
			this.cache[name] = new PIXI.Texture(
				this.texture,
				new PIXI.Rectangle(...[x, y, w, h].map(i=>i*this.size))
			);
		}

		return this.cache[name];
	}
}

return Tileset;

});