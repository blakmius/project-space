define(['libs/socket.io', './cookie'], (io, cookie) => {

const socket = io.connect(document.location.host);

socket.on('message', data => {
	let {text, timeout, autoHide, icon, heading} = data;
	$.toast({
		text, icon,
		hideAfter: autoHide ? false : timeout || 2000,
		heading: heading || 'Server',
		position: 'top-right'
	});
})

return socket;

});